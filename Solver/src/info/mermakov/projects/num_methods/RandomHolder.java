package info.mermakov.projects.num_methods;

import java.util.Random;

class RandomHolder {
    public final static Random random = new Random(System.currentTimeMillis());
}
