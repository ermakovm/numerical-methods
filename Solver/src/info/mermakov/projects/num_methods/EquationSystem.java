package info.mermakov.projects.num_methods;

import java.util.Random;

class EquationSystem {

    private final static Random random = RandomHolder.random;
    private final static double GRADIENT_DESCENT_PRECISION = 1e-6;

    private static double getNorm(double[] x) {
        double norm = Math.abs(x[0]);
        for (int i = 1; i < x.length; i++) {
            if (Math.abs(x[i]) > norm) {
                norm = Math.abs(x[i]);
            }
        }
        return norm;
    }

    private static double gradientDescent(Function f, double x0, double initialStep, double precision) {
        double[] arg = new double[1];
        arg[0] = x0;
        double derivative = f.totalDerivative(arg)[0];
        double step = initialStep, x = x0, x1, min = f.calculate(arg);
        while (step > precision) {
            if (derivative < 0) {
                x1 = x + step;
            } else {
                x1 = x - step;
            }
            arg[0] = x1;
            double cur = f.calculate(arg);
            if (cur < min) {
                min = cur;
                x = x1;
                arg[0] = x;
                derivative = f.totalDerivative(arg)[0];
            } else {
                step /= 2;
            }
        }
        return x;
    }

    private Function[] functions;
    private int n;

    public EquationSystem(Function[] functions) {
        this.functions = functions;
        n = functions.length;
    }

    double discrepancy(double[] x) {
        double discrepancy = 0, q;
        for (int i = 0; i < n; i++) {
            q = functions[i].calculate(x);
            discrepancy += q * q;
        }
        return discrepancy;
    }

    double discrepancy(double[] x0, double[] d, double t) {
        double[] x = new double[n];
        for (int i = 0; i < n; i++) {
            x[i] = x0[i] + t * d[i];
        }
        return discrepancy(x);
    }

    double[] linearDerivativeSolution(double[] x) {
        double[] b = new double[n];
        double[][] matrix = new double[n][];
        for (int i = 0; i < n; i++) {
            b[i] = -functions[i].calculate(x);
            matrix[i] = functions[i].totalDerivative(x);
        }
        Matrix m = new Matrix(matrix);
        return m.gaussMethod(b);
    }

    double localMinimum(final double[] x, final double[] d) {
        double cur = discrepancy(x);
        cur = Math.min(cur, discrepancy(x, d, 1));
        double r = 1, dr;
        do {
            r *= 2;
            dr = discrepancy(x, d, r);
            cur = Math.min(cur, dr);
        } while (dr <= cur);
        Function f = new Function() {
            @Override
            public double calculate(double[] arg) {
                return discrepancy(x, d, arg[0]);
            }
        };
        return gradientDescent(f, 1, 0.5, GRADIENT_DESCENT_PRECISION);
    }

    public double[] universalMethod(double eps, long maxIterations) {
        double[] x = new double[n];
        for (int i = 0; i < n; i++) {
            x[i] = random.nextDouble();
        }
        for (int q = 0; q < maxIterations; q++) {
            double [] dx = linearDerivativeSolution(x);
            double k = localMinimum(x, dx);
            for (int i = 0; i < n; i++) {
                x[i] += k * dx[i];
            }
            if (getNorm(dx) < eps) break;
        }
        return x;
    }
}
