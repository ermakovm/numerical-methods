﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Visualizer
{
    class FileUtil
    {
        public const string FileRes = "plot";
        private const string Res = ".plot";

        public static string[] FilesToPlot(string dir)
        {
            var files = Directory.GetFiles(dir, "*" + Res);
            return files;
        }
    }
}
