﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Visualizer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var filesToPlot = FileUtil.FilesToPlot(".");
            foreach (var tpPage in filesToPlot.Select(GenPage))
            {
                tabPlotMain.TabPages.Add(tpPage);
            }
        }

        private static TabPage GenPage(string name)
        {
            var visData = GetVisualData(name);
            var tpPage = new TabPage(visData["tab_name"]);
            GetPlotData(name);
            var data = GetPlotData(name);
            var area = new ChartArea("chart_file");
            area.AxisX.Name = "123";
            var chart = new Chart();
            var legend = new Legend();
            legend.Name = "legend_file";
            legend.Title = visData["chart_name"];
            legend.Alignment = StringAlignment.Center;
            chart.ChartAreas.Add("chart_file");
            chart.Legends.Add(legend);

            chart.Series.Add("  ");
            chart.Series[0].ChartArea = "chart_file";
            chart.Series[0].Legend = "legend_file";
            chart.Series[0].ChartType = SeriesChartType.Spline;
            for (var i = 0; i < data[0].Count; i++)
            {
                chart.Series[0].Points.AddXY(data[0][i], data[1][i]);
            }
            chart.Dock = DockStyle.Fill;
            chart.DataBind();
            tpPage.Controls.Add(chart);
            return tpPage;
        }

        private static Dictionary<string, string> GetVisualData(string name)
        {
            var sr = new StreamReader(name + ".vis");
            var dictionary = new Dictionary<string, string>();
            while (!sr.EndOfStream)
            {
                var temp = sr.ReadLine().Trim().Split(new[] {'\t'}, StringSplitOptions.RemoveEmptyEntries);
                dictionary.Add(temp[0], temp[1]);
            }
            sr.Close();
            return dictionary;
        }

        private static List<List<double>> GetPlotData(string name)
        {
            var res = new List<List<double>>();
            var x = new List<double>();
            var y = new List<double>();
            var sr = new StreamReader(name);
            while (!sr.EndOfStream)
            {
                var temp = sr.ReadLine()
                    .Trim()
                    .Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
                var a = Convert.ToDouble(temp[0]);
                var b = Convert.ToDouble(temp[1]);
                x.Add(a);
                y.Add(b);
            }
            sr.Close();
            res.Add(x);
            res.Add(y);
            return res;
        }
    }
}