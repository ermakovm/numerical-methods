﻿namespace Visualizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPlotMain = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // tabPlotMain
            // 
            this.tabPlotMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPlotMain.Location = new System.Drawing.Point(0, 0);
            this.tabPlotMain.Name = "tabPlotMain";
            this.tabPlotMain.SelectedIndex = 0;
            this.tabPlotMain.Size = new System.Drawing.Size(1099, 623);
            this.tabPlotMain.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 623);
            this.Controls.Add(this.tabPlotMain);
            this.Name = "Form1";
            this.Text = "Visualizer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPlotMain;
    }
}

